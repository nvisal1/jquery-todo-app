// Check off specfic todos by clicking on them
$('ul').on('click', 'li', function () {
	$(this).toggleClass('selected');
});

// Delete specfic todos by clicking respective delete button
$('ul').on('click', 'span', function (e) {
	$(this).parent().fadeOut(400, function () {
		$(this).remove();
	});
	// We only want to click the delete button here. 
	// We need to prevent the parent from being clicked because it 
	// triggers the toggle of the 'selected' class.
	e.stopPropagation();
});

// When the user presses the 'enter' key in the input field, 
// the content of the input field should be appended to the list 
// as a new todo. The content within the input field should also be deleted.
$("input[type='text']").keypress(function (e) {
	// grab input value 
	const textContent = $(this).val();
	if (e.which === 13) {
		// Check to ensure the input field is not empty.
		if (textContent == '') {
			alert('Please enter content into input field before submitting.');
		} else {
			// append input value into list of todos
			$('ul').append('<li><span><i class="fas fa-trash-alt"></i></span> ' + textContent + '</li>'); 
			// remove input field content
			$(this).val('');
		}
	}
});

// Clicking the plus icon on the top left should toggle the visibility 
// of the input field.

$('.fa-plus').on('click', function () {
	$("input[type='text']").fadeToggle(300);
});